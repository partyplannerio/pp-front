import React from 'react';
import { FlexBoxFullCenteredStyles } from '@shared/styles';
import styled from '@emotion/styled';
import { Result, Button } from 'antd';
import { NextFunctionComponent } from 'next';
import { ResultStatusType } from 'antd/lib/result';
import Link from 'next/link';

const Wrapper = styled.div`
  width: 100vw;
  height: 100vh;
  max-height: 100vh;
  min-height: 100vh;
  ${FlexBoxFullCenteredStyles};
`;

const defaultStatusCode = 404;

function getTitleSubTitle(statusCode: string) {
  if (statusCode == '404')
    return {
      title: '404',
      subTitle: 'Sorry, the page you visited does not exist.'
    };
  return {
    title: '500',
    subTitle: 'Something wrong happened.'
  };
}
const ErrorPage: NextFunctionComponent<{ statusCode: number }> = ({
  statusCode
}) => {
  const code = String(statusCode);
  return (
    <Wrapper>
      <Result
        {...getTitleSubTitle(code)}
        status={code as ResultStatusType}
        extra={
          <Button type="primary">
            <Link href="/">Back Home</Link>
          </Button>
        }
      />
    </Wrapper>
  );
};

ErrorPage.getInitialProps = ({ res, err }) => {
  const statusCode = res
    ? res.statusCode
    : err
    ? (err as any).statusCode
    : defaultStatusCode;
  return { statusCode };
};

export default ErrorPage;
