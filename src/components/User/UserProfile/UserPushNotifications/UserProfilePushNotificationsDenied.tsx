import React from 'react';
import { Result, Icon } from 'antd';
import styled from '@emotion/styled';

const DeniedResult = styled(Result)`
  padding: 0;
  .ant-result-icon {
    margin-bottom: 0;
  }
  .anticon.anticon-notification {
    font-size: 32px;
  }
`;

export default function UserProfilePushNotificationsDenied() {
  return (
    <DeniedResult
      icon={<Icon type="notification" />}
      title="Permission denied"
      subTitle="You have denied us the rights to sent you push notifications. Change your browser settings to enable this panel."
    />
  );
}
